# MeanFlagColorJS

Just a tool to visualize every country flag's mean color. 

Everything is alrealy available in `data`, but you can still regenerate everything:  
`npm i`  
`node index.js`

Depends on [Jimp](https://www.npmjs.com/package/jimp).
