const util = require("util");
const https = require("https");
const fs = require("fs");
const Jimp = require("jimp");

process.chdir(__dirname);

function logStatus(icon, status, message) {
  console.log(`[${icon} ${status}] ${message}`);
}

async function downloadFile(url, path, status = "Setup") {
  // Checking if file doesn't already exist
  if (await fs.promises.stat(path).catch(e => {}))
    return;

  logStatus("🔽", status, `Downloading file '${url}' as '${path}'...`);
  
  return new Promise((resolve, reject) => {
    https.get(url, res => {
      const file = fs.createWriteStream(path);
      res.pipe(file);
      file.on("finish", async () => {
        await file.close();
        resolve();
      });
    });
  });
}

(async () => {
  await fs.promises.mkdir("data", {recursive: true});

  await downloadFile("https://flagcdn.com/fr/codes.json", "data/country_codes.json");
  const arrowImage = await Jimp.read("resources/arrow.png");
  const font = await Jimp.loadFont(Jimp.FONT_SANS_64_BLACK);
  const countryCodes = require("./data/country_codes.json");

  await fs.promises.mkdir("data/flags", {recursive: true});
  await fs.promises.mkdir("data/mean_colors", {recursive: true});
  await fs.promises.mkdir("data/final_pictures", {recursive: true});

  const numberOfCountries = Object.keys(countryCodes).length;
  for (let i = 0; i < numberOfCountries; i++) {
    const status = `${i+1} / ${numberOfCountries}`;
    const countryCode = Object.keys(countryCodes)[i];
    const countryName = countryCodes[countryCode];
    const flagFileName = `data/flags/${countryCode}.png`;
    const meanColorFileName = `data/mean_colors/${countryCode}.png`;
    const finalImageFileName = `data/final_pictures/${countryName}.png`;

    if (await fs.promises.stat(finalImageFileName).catch(e => {})) {
      logStatus("⏩", status, `Flag of ${countryName} already exists, skipping...`);
      continue;
    }

    await downloadFile(`https://flagcdn.com/w1280/${countryCode}.png`, flagFileName, status);

    logStatus("🎨", status, `Creating mean flag color image '${meanColorFileName}'...`);
    const flagImage = await Jimp.read(flagFileName);
    let rgb = [[],[],[]];
    for (let i = 0; i < flagImage.bitmap.data.length; i++) {
      if (i % 4 != 3) rgb[i % 4].push(flagImage.bitmap.data[i]);
      else if (flagImage.bitmap.data[i] != 255) {
        rgb[0].pop();
        rgb[1].pop();
        rgb[2].pop();
      }
    }

    const meanColor = rgb.map(el => Math.round(el.reduce((acc, el) => acc + el) / el.length));
    const intMeanColor = Jimp.rgbaToInt(meanColor[0], meanColor[1], meanColor[2], 255);
    const meanColorImage = await new Jimp(200, 200, intMeanColor);
    await meanColorImage.write(meanColorFileName);

    logStatus("💾", status, `Creating final image '${finalImageFileName}'...`);
    const finalImage = await new Jimp(flagImage.bitmap.width + 300, flagImage.bitmap.height + 100, 0xDDDDDDFF);
    finalImage.composite(flagImage, 0, 100);
    finalImage.composite(arrowImage, flagImage.bitmap.width + 3, 100 + Math.round(flagImage.bitmap.height / 2) - 21);
    finalImage.composite(meanColorImage, flagImage.bitmap.width + 100, 100 + Math.round(flagImage.bitmap.height / 2) - 100);
    finalImage.print(font, 0, 0, {
        text: countryName,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
      }, flagImage.bitmap.width + 200, 100);
    await finalImage.write(finalImageFileName);
  }
})();
